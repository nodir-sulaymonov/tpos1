import argparse
import libtmux
import tqdm
import os


server = libtmux.Server()

def start(num_users, base_dir, session_name):
    """
    Запустить $num_users ноутбуков. У каждого рабочай директория $base_dir+$folder_num
    """
    ip = "127.0.0.1"
    session = server.new_session(session_name)
    for user in tqdm.tqdm(range(num_users)):
        window = session.new_window()
        pane = window.split_window()
        port = 10000 + user
        user_dir = os.path.join(os.path.abspath(base_dir), str(user))
        if not os.path.exists(user_dir):
            os.mkdir(user_dir)
        pane.send_keys("cd " + user_dir)
        token = "usr" + str(user)
        pane.send_keys("jupyter notebook --ip {} --port {} --no-browser --NotebookApp.token='{}' --NotebookApp.notebook_dir='{}'".format(ip, port, token, user_dir))


def stop(session_name, num):
    """
    @:param session_name: Названия tmux-сессии, в которой запущены окружения
    @:param num: номер окружения, кот. можно убить
    """
    session = server.find_where({"session_name" : session_name})
    session.kill_window(num + 1)


def stop_all(session_name):
    """
    @:param session_name: Названия tmux-сессии, в которой запущены окружения
    """
    server.kill_session(session_name)



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("operation", type=str, help="start/stop/stop_all")
    parser.add_argument("-s", "--session_name", default='default_session', type=str, help="session name")
    parser.add_argument("-d", "--base_directory", default='./', type=str, help="base directory")
    parser.add_argument("-n", "--num", type=int, help="number of sessions")
    parser.add_argument("-nu", "--num_users", type=int, help="number of users")
    args = parser.parse_args()

    if args.operation == 'start':
        start(args.num_users, args.base_directory, args.session_name)
    elif args.operation == 'stop':
        stop(args.session_name, args.num)
    elif args.operation == 'stop_all':
        stop_all(args.session_name)
    else:
        print("Available operations are start, stop, or stop_all")